﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float horizontalMovement;
    private float verticleMovement;

    [Range(0, 1)]
    public float movementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMovement = Input.GetAxis("Horizontal");
        verticleMovement = Input.GetAxis("Vertical");
        if (horizontalMovement != 0)
        {
            MoveHorizontal(horizontalMovement);
        }
        if (verticleMovement != 0)
        {
            MoveVertical(verticleMovement);
        }

    }

    private void MoveHorizontal(float Direction)
    {
        this.transform.Translate((Vector3.right * Direction) * movementSpeed * Time.deltaTime);
    } 

    private void MoveVertical(float Direction)
    {
        this.transform.Translate((Vector3.up * Direction) * movementSpeed * Time.deltaTime);
    }
}
